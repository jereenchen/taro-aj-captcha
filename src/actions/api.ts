import api from '../service/api'


// 获取滑块图片等信息
export function getPicture(data:any) {
    return api.post(`/captcha/get`,data,'application/json')
  }
  
  // /captcha/check 验证是否成功
  export function reqCheck(data:any) {
    return api.post(`/captcha/check`,data,'application/json')
  }
  