import Taro from '@tarojs/taro'
import { HTTP_STATUS } from '../utils/status'
import { baseUrl } from '../config'
import { logError } from '../utils/error'

export default {
  baseOptions(params, method = 'GET') {
    var isTokenValid = true; 
    let { url, data } = params
    let contentType = 'application/json; charset=UTF-8'
    // var token = Taro.getStorageSync('token')
    contentType = params.contentType || contentType
    type OptionType = {
      url: string,
      data?: object | string,
      method?: any,
      header: object,
      // mode: string,
      success: any,
      error: any,
      xhrFields: object,
    }
    const setCookie = (res: {
      cookies: Array<{
        name: string,
        value: string,
        expires: string,
        path: string
      }>,
      header: {
        'Set-Cookie': string
      }
    }) => {
      if (res.cookies && res.cookies.length > 0 && url.indexOf('login/cellphone') !== -1) {
        // console.info("res ===>", res)
        let cookies = '';
        res.cookies.forEach((cookie, index) => {
          // windows的微信开发者工具返回的是cookie格式是有name和value的,在mac上是只是字符串的
          if (cookie.name && cookie.value) {
            cookies += index === res.cookies.length - 1 ? `${cookie.name}=${cookie.value};expires=${cookie.expires};path=${cookie.path}` : `${cookie.name}=${cookie.value};`
          } else {
            cookies += `${cookie}`
          }
        });
        Taro.setStorageSync('cookies', cookies)
      }
    }
    data = {
      ...data,
    } 

    const option: OptionType = {
      // url: url.indexOf('http') !== -1 ? url : baseUrl + url,
      url:baseUrl + url,
      data: data,
      method: method,
      header: {
        'content-type': contentType,
      //   // cookie: Taro.getStorageSync('cookies')
        // 'Authorization':`Bearer ${token}`
        'X-Requested-With': 'XMLHttpRequest',
      },
      // mode: 'cors',
      xhrFields: { withCredentials: true },
  
      success(res) {
        console.log('res', res)
        setCookie(res)
        if (res.statusCode === HTTP_STATUS.NOT_FOUND) {
          return logError('api', '请求资源不存在')
        } else if (res.statusCode === HTTP_STATUS.BAD_GATEWAY) {
          return logError('api', '服务端出现了问题')
        } else if (res.statusCode === HTTP_STATUS.FORBIDDEN) {
          return logError('api', '没有权限访问')
        } else if (res.statusCode === HTTP_STATUS.AUTHENTICATE) {
          Taro.clearStorage()
          Taro.navigateTo({
            url: "/packageA/pages/vrlogin/index"
          });
          return logError('api', '请先登录')
        }else if(res.data.code===401){
          console.log(isTokenValid,'isPrompted1');
          if (!isTokenValid) {
            return; // 如果isTokenValid为false，则不执行其他接口请求
          }
          isTokenValid = false; // 设置为false，阻止其他接口执行
          console.log(isTokenValid,'isPrompted2');
          // var token = Taro.getStorageSync('token')
          Taro.showModal({
            title: '提示',
            confirmColor:'#e2000f',
            content: token?'登录已过期，请重新登录':'请登录使用丹佛斯中国服务小程序',
            cancelText:'拒绝',
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
               
              } else if (res.cancel) {
                console.log('用户点击取消')
                isTokenValid = true;
              }
            }
          });
          return logError('api', '请先登录')
        } else if(res.statusCode === HTTP_STATUS.GATEWAY_TIMEOUT){
          Taro.showToast({
                title: '请求超时',
                icon: 'error',
              });
          return logError('api', '请求超时')
        } else if (res.statusCode === HTTP_STATUS.SUCCESS) {
          return res.data
        }
      },
      error(e) {
        console.log('api', '请求接口出现问题', e);
        
        logError('api', '请求接口出现问题', e)
      }
    }
    // eslint-disable-next-line
    return Taro.request(option)
  },
  get(url, data?: object) {
    let option = { url, data }
    return this.baseOptions(option)
  },
  post: function (url, data?: object, contentType?: string) {
    let params = { url, data, contentType }
    return this.baseOptions(params, 'POST')
  },
  put(url, data?: object) {
    let option = { url, data }
    return this.baseOptions(option, 'PUT')
  },
  delete(url, data?: object) {
    let option = { url, data }
    return this.baseOptions(option, 'DELETE')
  },
}
