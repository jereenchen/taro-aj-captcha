/**
 * @name ImageCode
 * @desc 滑动拼图验证
 * @author 陈杰伦
 * @version 2024-02-22
 * 
 * @param {String} imageUrl 图片的路径
 * @param {String} top 距离顶部的高度
 * @param {Function} onReload 当点击'重新验证'时执行的函数
 * @param {Function} onMath 滑动结束后进行验证的函数,接收两个参数，第一个是滑动结束的x坐标，第二个是验证失败执行的回调函数
 * @param {Function} onClose 点击关闭按钮时执行的函数
 */
import React, { Component, useState, useEffect, useRef } from 'react';
import Taro from '@tarojs/taro'
import { View, Text, Canvas, Image, MovableView, MovableArea } from '@tarojs/components'
// import './index.scss'
import close_image from './icon/close.png'
import reload_image from './icon/reload.png'

import { CSSTransition } from 'react-transition-group';
import { getPicture, reqCheck } from '../../actions/api.ts';
import { aesEncrypt } from "../../utils/ase.js";
import defaultImg from './icon/default.jpg';
import './assets/index.css';

class ImageCode extends Component {
    constructor(props) {
        super(props);
        this.state = {
            blockSize: {
                width: '50px',
                height: '50px'
            },
            setSize: {
                imgHeight: 200,
                imgWidth: 380,
                barHeight: 40,
                barWidth: 310,
            },
            backImgBase: '',
            blockBackImgBase: '',
            backToken: '',
            startMoveTime: "",
            endMovetime: '',
            tipsBackColor: '',
            secretKey: '',
            captchaType: 'blockPuzzle',
            moveBlockBackgroundColor: 'rgb(255, 255, 255)',
            leftBarBorderColor: '',
            iconColor: '',
            barAreaLeft: 0,
            barAreaOffsetWidth: 0,
            startLeft: null,
            moveBlockLeft: null,
            leftBarWidth: null,
            status: false,
            isEnd: false,
            passFlag: '',
            tipWords: '',
            text: '向右滑动完成验证',
        }
    }

    componentDidMount() {
        this.uuid();
        this.init();

        // 组件卸载时移除事件监听
        Taro.eventCenter.off('touchmove', this.touchMoveHandler);
        Taro.eventCenter.off('touchend', this.touchEndHandler);

        // 调用 setBarArea 方法
        setTimeout(() => {
            this.setBarArea(this.bararea);
        }, 0);
    }


    // 初始化 uuid 
    uuid() {
        var s = [];
        var hexDigits = "0123456789abcdef";
        for (var i = 0; i < 36; i++) {
            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        }
        s[14] = "4";
        s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
        s[8] = s[13] = s[18] = s[23] = "-";

        var slider = 'slider' + '-' + s.join("");
        var point = 'point' + '-' + s.join("");

        if (!Taro.getStorageSync('slider')) {
            Taro.setStorageSync('slider', slider);
        }
        if (!Taro.getStorageSync('point')) {
            Taro.setStorageSync("point", point);
        }
    }

    init() {
        this.getData();
    }

    getData() {
        getPicture({ captchaType: this.state.captchaType, clientUid: Taro.getStorageSync('slider'), ts: Date.now() }).then(res => {
            if (res.data.repCode === '0000') {
                console.log(res.data.repData, 'res.data.repData');
                // 在组件中使用转换后的URL加载图片
                // const imgUrl = base64ToURL(res.data.repData.originalImageBase64);
                // console.log(imgUrl,'imgUrl');
                this.setState({
                    backImgBase: res.data.repData.originalImageBase64,
                    blockBackImgBase: res.data.repData.jigsawImageBase64,
                    backToken: res.data.repData.token,
                    secretKey: res.data.repData.secretKey
                })
            }
            // 请求次数超限
            if (res.data.repCode === '6201') {
                this.setState({
                    backImgBase: null,
                    blockBackImgBase: null,
                    leftBarBorderColor: '#d9534f',
                    iconColor: '#fff',
                    iconClass: 'icon-close',
                    passFlag: false,
                    tipWords: res.data.repMsg
                });
                setTimeout(() => {
                    this.setState({
                        tipWords: ''
                    });
                }, 1000);
            }
        });
    }

    refresh = () => {
        this.getData();
        this.setState({
            moveBlockLeft: '',
            leftBarWidth: '',
            text: '向右滑动完成验证',
            moveBlockBackgroundColor: '#fff',
            leftBarBorderColor: '#337AB7',
            iconColor: '#fff',
            status: false,
            isEnd: false
        });
    }


    setBarArea = (bararea) => {
        if (bararea) {
          Taro.createSelectorQuery().select('.verify-bar-area').boundingClientRect(rect => {
            console.log('setBarArea',rect.left,rect.width);
            if (rect) {
              this.setState({
                barAreaLeft: rect.left,
                barAreaOffsetWidth: rect.width
              });
            }
          }).exec();
        }
      }
      

   // 鼠标按下时的事件处理函数
start = (e) => {
    console.log(e,'按下');
    this.state.startMoveTime = new Date().getTime();   //记录开始滑动的时间
    if (!this.state.isEnd) {
        this.setState({
            text: '',
            moveBlockBackgroundColor: '#337ab7',   //移动块背景颜色
            leftBarBorderColor: '#337AB7',   //左侧滑块边框颜色
            iconColor: '#fff',   //图标颜色
            status: true   //状态标识为true
        });
        e.stopPropagation();   //阻止事件冒泡
    }
}

// 鼠标移动时的事件处理函数
move = (e) => {
    console.log(e,'滑动滑动');
        if (this.state.status && !this.state.isEnd) {
            console.log(e.touches[0].pageX);
            let x =  e.touches[0].pageX ;   // 获取鼠标/触摸点的X坐标
            let bar_area_left = this.state.barAreaLeft;   // 滑块区域左侧距离
            let move_block_left = x - bar_area_left;   // 计算移动块的左侧距离
            console.log(move_block_left >= this.state.barAreaOffsetWidth - parseInt(parseInt(this.state.blockSize.width) / 2) - 2);
            if (move_block_left >= this.state.barAreaOffsetWidth - parseInt(parseInt(this.state.blockSize.width) / 2) - 2) {
                console.log('第一个判断计算');
                move_block_left = this.state.barAreaOffsetWidth - parseInt(parseInt(this.state.blockSize.width) / 2) - 2;
            }
            if (move_block_left <= 0) {
                console.log('第二个判断计算');
                move_block_left = parseInt(this.state.blockSize.width / 2);
            }
            let moveBlockLeft = Math.floor(move_block_left - this.state.startLeft) + "px";   //计算移动块的左侧位置并取整
            let leftBarWidth = Math.floor(move_block_left - this.state.startLeft) + "px";   //计算左侧滑块的宽度并取整
            console.log(moveBlockLeft, leftBarWidth);   //打印移动块左侧位置和左侧滑块宽度
            this.setState({
                moveBlockLeft: moveBlockLeft,   //更新移动块左侧位置
                leftBarWidth: leftBarWidth   //更新左侧滑块宽度
            });
        }
}

    end = () => {
        console.log('鼠标放下');
        let endMovetime = +new Date();
        if (this.state.status && !this.state.isEnd) {
            let moveLeftDistance = parseInt((this.state.moveBlockLeft || '').replace('px', ''));
            moveLeftDistance = moveLeftDistance * 310 / parseInt(this.state.setSize.imgWidth);
            let data = {
                captchaType: this.state.captchaType,
                "pointJson": this.state.secretKey ? aesEncrypt(JSON.stringify({ x: moveLeftDistance, y: 5.0 }), this.state.secretKey) : JSON.stringify({ x: moveLeftDistance, y: 5.0 }),
                // "pointJson": this.state.secretKey ? (JSON.stringify({ x: moveLeftDistance, y: 5.0 }), this.state.secretKey) : JSON.stringify({ x: moveLeftDistance, y: 5.0 }),
                "token": this.state.backToken,
                clientUid: Taro.getStorageSync('slider'),
                ts: Date.now()
            };
            reqCheck(data).then(res => {
                if (res.data.repCode === "0000") {
                    this.setState({
                        isEnd: true,
                        passFlag: true,
                        tipWords: `${((endMovetime - this.state.startMoveTime) / 1000).toFixed(2)}s验证成功`
                    });
                    setTimeout(() => {
                        this.setState({
                            tipWords: ''
                        });
                        var captchaVerification = this.state.secretKey  ?aesEncrypt(this.state.backToken+'---'+JSON.stringify({x:moveLeftDistance,y:5.0}),this.state.secretKey):this.state.backToken+'---'+JSON.stringify({x:moveLeftDistance,y:5.0})
                        // this.props.handleClick(captchaVerification);
                        this.refresh();
                    }, 1000);
                } else {
                    this.setState({
                        isEnd: true,
                        moveBlockBackgroundColor: '#d9534f',
                        leftBarBorderColor: '#d9534f',
                        iconColor: '#fff',
                        iconClass: 'icon-close',
                        passFlag: false,
                        tipWords: res.data.repMsg || '验证失败'
                    });
                    setTimeout(() => {
                        this.refresh();
                        this.setState({
                            tipWords: ''
                        });
                    }, 1000);
                }
            });
            this.setState({
                status: false
            });
        }
    }


    render() {
        const { mode, captchaType, vSpace, barSize, showRefresh, transitionWidth, finishText, transitionLeft } = this.props;
        return (
            <View style={{ position: 'relative' }} className='stop-user-select'>
                <View
                    className='verify-img-out'
                    style={{ height: parseInt(this.state.setSize.imgHeight) + vSpace }}
                >
                    <View
                        className='verify-img-panel'
                        style={{ width: this.state.setSize.imgWidth, height: this.state.setSize.imgHeight }}
                    >
                        {/* {this.state.backImgBase+'upUI么'} */}
                        <Image
                            src={'data:image/png;base64,' + this.state.backImgBase}
                            mode='aspectFill'
                            style={{ width: '100%', height: '100%'
                            // , backgroundImage: "url('data:image/png;base64," + this.state.backImgBase + "')" ,backgroundRepeat: 'round'
                        }}
                        />
                         {/* :
                         <Image 
                            src={defaultImg}
                            mode='aspectFill'
                            style={{ width: '100%', height: '100%' }}
                        />} */}
                        {/* 其他内容 */}
                        <CSSTransition in={this.state.tipWords.length > 0} timeout={150} classNames="tips" unmountOnExit>
                        <View
                            className={
                            this.state.passFlag
                                ? `${'verify-tips'} ${'suc-bg'}`
                                : `${'verify-tips'} ${'err-bg'}`
                            }
                        >
                            {this.state.tipWords}
                        </View>
                        </CSSTransition>
                    </View>
                </View>

                <View 
                    className='verify-bar-area'
                    style={{ width: this.state.setSize.imgWidth, height: barSize.height, lineHeight: barSize.height }}
                    // ref={(bararea) => this.setBarArea(bararea)}
                    ref={(ref) => this.bararea = ref}
                >
                    <Text className='verify-msg'>{this.state.text}</Text>
                    <View
                        className='verify-left-bar'
                        style={{
                            width: this.state.leftBarWidth !== undefined ? this.state.leftBarWidth : barSize.height,
                            height: barSize.height,
                            borderColor: this.state.leftBarBorderColor,
                            transaction: transitionWidth,
                        }}
                    >
                        <Text className='verify-msg'>{finishText}</Text>

                        <View
                            className='verify-move-block'
                            onTouchStart={this.start}
                            onMouseDown={this.start}
                            onTouchMove={this.move}
                            onTouchEnd={this.end}
                            style={{
                                width: barSize.height,
                                height: barSize.height,
                                backgroundColor: this.state.moveBlockBackgroundColor,
                                left: this.state.moveBlockLeft,
                                transition: transitionLeft,
                            }}
                        >
                            <Text
                                className='verify-icon iconfont icon-right'
                                style={{ color: this.state.iconColor }}
                            ></Text>
                            <View
                                className='verify-sub-block'
                                style={{
                                    width: Math.floor((parseInt(this.state.setSize.imgWidth) * 47) / 310) + 'px',
                                    height: this.state.setSize.imgHeight,
                                    top: '-' + (parseInt(this.state.setSize.imgHeight) + vSpace) + 'px',
                                    backgroundSize: this.state.setSize.imgWidth + ' ' + this.state.setSize.imgHeight,
                                }}
                            >
                                <Image
                                    src={'data:image/png;base64,' + this.state.blockBackImgBase}
                                    alt=""
                                    style={{ width: '100%', height: '100%', display: 'block' }}
                                />
                            </View>
                        </View>
                    </View>
                </View>

            </View>
        )
    }
}

ImageCode.defaultProps = {
    mode: 'fixed',
    vSpace: 5,
    imgSize: {
        width: '310px',
        height: '200px',
    },
    barSize: {
        width: '310px',
        height: '40px',
    },
    setSize: {
        imgHeight: 200,
        imgWidth: 310,
        barHeight: 0,
        barWidth: 0,
    }
};

export default ImageCode;

