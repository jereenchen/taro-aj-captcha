// export const baseUrl: string = process.env.GQL_ENDPOINT || '' // 这里配置的这个url是后端服务的请求地址，示例中代表在本地启用的服务端口是3000，如果希望在真机上调试，那么就需要将后端服务部署到一个云主机上

let baseUrlPrefix = ''
const env = process.env.NODE_ENV === 'development' ? 'development' : (process.env.NODE_ENV === 'Test' ? 'Test' : 'production')
console.log(process.env.NODE_ENV)
switch (env) { 
    case 'development':
        baseUrlPrefix = 'http://127.0.0.1:8080'
        break;
    case 'Test':
        baseUrlPrefix = ''
        break;
    case 'production':
        baseUrlPrefix = ''
        break
}
export const baseUrl = baseUrlPrefix