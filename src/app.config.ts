export default defineAppConfig({
  pages: [
    'pages/index/index'
  ],
  // "plugins": {
  //   "player": {
  //      "version": "2.4.3",
  //      "provider": "wx729a0567e8bce67a"
  //   }
  // },
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  }
})
