# Taro-AJ-Captcha

#### 介绍
基于anji-plus / AJ-Captcha开源项目基础上实现的Taro开发小程序上的滑块验证码，由于Taro生态资源较少anji-plus / AJ-Captcha此开源项目唯独没有适配Taro框架的项目案例

#### 安装教程

1.  后端参考https://gitee.com/anji-plus/captcha?_from=gitee_search#https://gitee.com/link?target=https%3A%2F%2Fajcaptcha.beliefteam.cn%2Fcaptcha-doc%2F
2.  启动前端，使用visual code打开项目终端安装npm安装yarn后输入yarn安装依赖包，单独安装yarn add react-transition-group@4.4.1依赖后npm run build:weapp -- --watch启动项目
3.  小程序开发者工具导入项目文件测试号打开即可

```js
npm install -g yarn
yarn
yarn add react-transition-group@4.4.1
npm run dev
npm run build:weapp -- --watch
``` 

####- 效果图

![普通鸟](src/components/ImageCode/assets/Screenshot%202024-03-08%20134937.png)|![失败鸟](src/components/ImageCode/assets/Screenshot%202024-03-08%20142108.png)|![成功鸟](src/components/ImageCode/assets/Screenshot%202024-03-08%20142230.png)|![普通豚](src/components/ImageCode/assets/Screenshot%202024-03-08%20135136.png)